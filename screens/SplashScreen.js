import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/Feather';
import PickerComponent from './components/Picker';

const SplashScreen = ({navigation}) => {
  const [isVisible, setIsVisible] = useState(true);
  const [value, setValue] = useState('sender');

  setTimeout(() => {
    setIsVisible(false);
  }, 3000);

  return (
    <View style={styles.container}>
      <LinearGradient
        start={{x: 0.0, y: 0}}
        end={{x: 0.5, y: 1.2}}
        locations={[0, 0.5, 5]}
        colors={['white', '#c1bebf', '#abb8c3']}
        style={styles.linearGradient}>
        {isVisible == true ? (
          <View style={{position: 'absolute', top: heightPercentageToDP('30')}}>
            <Image
              source={require('../assets/images/L1.png')}
              style={styles.logo}
            />
            <View style={styles.ActivityIndicatorStyle}>
              <ActivityIndicator size="large" color="#a9071a" />
            </View>
          </View>
        ) : (
          <View
            style={{
              // borderWidth: 1,
              borderColor: 'red',
              height: heightPercentageToDP('70'),
            }}>
            <View>
              <View style={styles.logoContainer}>
                <Image
                  source={require('../assets/images/L1.png')}
                  style={styles.logo}
                />
              </View>
              <View>
                <View style={styles.titleContainer}>
                  <Text style={styles.logoStyle}>Login As</Text>
                </View>
                <View style={styles.formContainer}>
                  <View>
                    <View
                      style={[
                        styles.textInput,
                        {
                          // height: {changeHeight}
                        },
                      ]}>
                      <View
                        style={[
                          styles.loginAsContainer,
                          {
                            width: widthPercentageToDP('60'),
                          },
                        ]}>
                        <PickerComponent sendValue={setValue} />
                      </View>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    // borderWidth: 1,
                    height: heightPercentageToDP('15'),
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={
                      () =>
                        navigation.navigate('SignIn', {
                          paramKey: value,
                        })
                      // console.log(value)
                    }>
                    {value == 'sender' || value == 'courier' ? (
                      <Icon
                        name="arrow-right-circle"
                        color="#a9071a"
                        size={50}
                      />
                    ) : (
                      <Icon
                        name="arrow-right-circle"
                        color="#5d595a"
                        size={50}
                      />
                    )}
                    {/* <Icon name="arrow-right-circle" color="#a9071a" size={50} /> */}
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        )}
      </LinearGradient>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    // flex: 1
  },
  linearGradient: {
    width: widthPercentageToDP('100'),
    height: heightPercentageToDP('100'),
    justifyContent: 'center',
    alignItems: 'center',
    // flex: 1
  },
  logo: {},
  titleContainer: {
    alignItems: 'center',
    height: heightPercentageToDP('9'),
    // borderWidth: 1,
    justifyContent: 'center',
  },
  logoStyle: {
    // fontFamily: 'LibreBaskerville-Bold',
    fontSize: 30,
    color: '#a9071a',
    fontWeight: 'bold',
  },
  formContainer: {
    width: widthPercentageToDP('98'),
    height: heightPercentageToDP('20'),
    justifyContent: 'center',
    alignItems: 'center',
    // borderWidth: 1
  },
  logoContainer: {
    // borderWidth: 1,
    alignItems: 'center',
    width: widthPercentageToDP('98'),
    height: heightPercentageToDP('20'),
    justifyContent: 'center',
    // position: 'absolute',
    // bottom: heightPercentageToDP('44'),
    // right: widthPercentageToDP('23'),
  },
  textInput: {
    // borderWidth: 1,
    // backgroundColor: 'white',
    width: widthPercentageToDP('98'),
    height: heightPercentageToDP('25'),
    justifyContent: 'center',
    alignItems: 'center',
    // flexDirection: 'row',
  },
  linearGradientTextInput: {
    height: heightPercentageToDP('8'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonStyle: {
    fontSize: 20,
    color: '#a9071a',
  },
  ActivityIndicatorStyle: {
    position: 'absolute',
    top: heightPercentageToDP('30'),
    right: widthPercentageToDP('10'),
  },
  loginAsContainer: {
    // borderWidth: 1,
    // width: widthPercentageToDP('30'),
    alignItems: 'center',
  },
});
