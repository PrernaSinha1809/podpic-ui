import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {useState, useEffect} from 'react/cjs/react.development';
import axios from 'axios';

const FrgtUsername = ({navigation, route}) => {
  const [show, setShow] = useState(route.params.paramKey);
  const [formData, setFormData] = useState({
    email: '',
    nchoice: route.params.paramKey,
    password: '',
  });

  useEffect(() => {});

  const sendMail = () => {
    axios
      .post('http://208.82.115.154:8080/api/auth/rcrUsername', formData)
      .then(response => {
        // handle success
        navigation.navigate('SignIn');
      })
      .catch(error => {
        // handle error
        Alert.alert('Something Went Wrong!', error);
      });
  };
  return (
    <View
      style={{
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <LinearGradient
        start={{x: 0.0, y: 0}}
        end={{x: 0.5, y: 1.2}}
        locations={[0, 0.5, 5]}
        colors={['white', '#c1bebf', '#abb8c3']}
        style={styles.linearGradient}>
        <View style={styles.formContainer}>
          <View style={{}}>
            {show == 'username' ? (
              <TextInput
                placeholder="Enter email to retrieve username"
                placeholderTextColor="#cccccc"
                style={styles.textInput}
                onChangeText={text =>
                  setFormData({
                    ...formData,
                    email: text,
                  })
                }
              />
            ) : show == 'password' ? (
              <View>
                <TextInput
                  placeholder="Enter email to update password"
                  placeholderTextColor="#cccccc"
                  style={[styles.textInput]}
                  onChangeText={text => setFormData({...formData, email: text})}
                />
                <TextInput
                  placeholder="Enter new password"
                  placeholderTextColor="#cccccc"
                  style={styles.textInput}
                  onChangeText={text =>
                    setFormData({...formData, password: text})
                  }
                />
              </View>
            ) : (
              <TextInput
                placeholder="Enter email to retrieve username"
                placeholderTextColor="#cccccc"
                style={styles.textInput}
                onChangeText={text =>
                  setFormData({...formData, email: text, nchoice: 'username'})
                }
              />
            )}
          </View>
          <View style={{alignItems: 'center'}}>
            <TouchableOpacity
              style={{
                elevation: 10,
                marginTop: heightPercentageToDP('2'),
              }}
              onPress={() => sendMail()}>
              {show == 'username' ? (
                <Text style={styles.buttonStyle}>Send Email</Text>
              ) : (
                <Text style={styles.buttonStyle}>Set Password</Text>
              )}
            </TouchableOpacity>
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};
export default FrgtUsername;

const styles = StyleSheet.create({
  linearGradient: {
    width: widthPercentageToDP('100'),
    height: heightPercentageToDP('100'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainer: {
    width: widthPercentageToDP('98'),
    height: heightPercentageToDP('20'),
    alignItems: 'center',
    borderWidth: 1,
  },
  textInput: {
    width: widthPercentageToDP('80'),
    fontSize: 18,
    justifyContent: 'center',
  },
  linearGradientTextInput: {
    height: heightPercentageToDP('8'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonStyle: {
    fontSize: 20,
    color: '#a9071a',
  },
});
