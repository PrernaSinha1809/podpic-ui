import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
  TextInput,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Entypo';
import Icon1 from 'react-native-vector-icons/FontAwesome5';
import Icon2 from 'react-native-vector-icons/AntDesign';

const PDFViewer = ({route}) => {
  const navigation = useNavigation();
  const [imageArray, setImageArray] = useState([]);
  const [userInfo, setUserInfo] = useState({});
  const [readText, setReadText] = useState('');
  const [roles, setRole] = useState([]);
  const [roleName, setRoleName] = useState('');
  const [data, setData] = useState({
    status: 'In-Progress',
    signature: '',
  });

  useEffect(() => {
    setImageArray(route.params.imageInfo);
    setUserInfo(route.params.userInfo);
    setReadText(route.params.readText);
    setRoleName(route.params.roleName);
  });

  const logout = () => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    var raw = JSON.stringify({
      username: userInfo.username,
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch('http://208.82.115.154:8080/api/auth/signout', requestOptions)
      .then(response => response.json())
      .then(result => navigation.navigate('SignIn'))
      .catch(error => console.log('error', error));
  };

  const ImageTag = () => {
    // console.log(imagesState[0].path);
    return imageArray.map((value, index) => {
      return (
        <View key={index}>
          <View>
            <Image
              source={{uri: value.path}}
              style={{
                width: 100,
                height: 100,
                marginHorizontal: widthPercentageToDP('3'),
              }}
            />
          </View>
        </View>
      );
    });
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <LinearGradient
        start={{x: 0.0, y: 0}}
        end={{x: 0.5, y: 1.2}}
        locations={[0, 0.5, 5]}
        colors={['white', '#c1bebf', '#abb8c3']}
        style={styles.linearGradient}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.container1}>
            <View style={styles.titleBar}>
              {/* <View> */}
              <View style={styles.burgerIconContainer}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <Icon name="back" size={25} color="white" />
                </TouchableOpacity>
              </View>
              <View style={styles.welcContainer}>
                <Text style={styles.welcStyle}>Welcome</Text>
              </View>
              <View style={[styles.welcContainerNormal]}>
                <View>
                  <Text style={styles.userNStyle}>{userInfo.username}</Text>
                </View>
                <TouchableOpacity
                  onPress={() => logout()}
                  style={{
                    // borderWidth: 1,
                    width: widthPercentageToDP('15'),
                    alignItems: 'flex-end',
                  }}>
                  <Icon2 name="logout" size={25} color="white" />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {roleName == '"ROLE_SENDER"' ? (
            <View
              style={{
                width: widthPercentageToDP('95'),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  styles.readTextContainer,
                  {
                    width: widthPercentageToDP('95'),
                    height: heightPercentageToDP('40'),
                  },
                ]}>
                <ScrollView>
                  <Text style={{fontSize: 25}}>{readText}</Text>
                </ScrollView>
              </View>
              {imageArray != null ? (
                <View
                  style={{
                    flexDirection: 'row',
                    // borderWidth: 1,
                  }}>
                  <ScrollView horizontal={true}>{ImageTag()}</ScrollView>
                </View>
              ) : null}
            </View>
          ) : roleName == '"ROLE_TRANSPORTER/SHIPPER"' ? (
            <View
              style={{
                width: widthPercentageToDP('95'),

                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  styles.readTextContainer,
                  {
                    width: widthPercentageToDP('95'),
                    height: heightPercentageToDP('40'),
                  },
                ]}>
                <ScrollView>
                  <Text style={{fontSize: 25}}>{readText}</Text>
                </ScrollView>
              </View>
              {imageArray != null ? (
                <View
                  style={{
                    flexDirection: 'row',
                    // borderWidth: 1,
                  }}>
                  <ScrollView horizontal={true}>{ImageTag()}</ScrollView>
                </View>
              ) : null}
            </View>
          ) : (
            <View
              style={{
                width: widthPercentageToDP('95'),
                // height: heightPercentageToDP('75'),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  styles.readTextContainer,
                  {
                    width: widthPercentageToDP('95'),
                    height: heightPercentageToDP('40'),
                  },
                ]}>
                <ScrollView>
                  <Text style={{fontSize: 25}}>{readText}</Text>
                </ScrollView>
              </View>
              {imageArray != null ? (
                <View
                  style={{
                    flexDirection: 'row',
                    // borderWidth: 1,
                  }}>
                  <ScrollView horizontal={true}>{ImageTag()}</ScrollView>
                </View>
              ) : null}
            </View>
          )}
          <View>
            <TouchableOpacity
              style={styles.buttonContainer}
              onPress={() =>
                navigation.navigate('StatusHandle', {
                  userInfo: userInfo,
                  roleName: roleName,
                  trackNo: route.params.trackNo,
                })
              }>
              <Text style={{color: 'white', fontSize: 20}}>Proceed</Text>
            </TouchableOpacity>
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};
export default PDFViewer;

const styles = StyleSheet.create({
  linearGradient: {
    width: widthPercentageToDP('100'),
    height: heightPercentageToDP('100'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    paddingTop: heightPercentageToDP('6'),
  },
  scrollView: {
    // backgroundColor: 'pink',
    // borderWidth: 1,
    marginHorizontal: widthPercentageToDP('1'),
  },
  text: {
    fontSize: 42,
  },
  linearGradientTextInput: {
    height: heightPercentageToDP('10'),
    justifyContent: 'center',
    alignItems: 'center',
    width: widthPercentageToDP('90'),
  },
  titleBar: {
    // borderWidth: 1,
    width: widthPercentageToDP('95'),
    height: heightPercentageToDP('10'),
    backgroundColor: '#a9071a',
    borderRadius: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  burgerIconContainer: {
    justifyContent: 'center',
    width: widthPercentageToDP('10'),
    alignItems: 'center',
  },
  welcContainerNormal: {
    // borderWidth: 1,
    // borderColor: 'white',
    justifyContent: 'center',
    width: widthPercentageToDP('40'),
    height: heightPercentageToDP('10'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  welcStyle: {
    color: 'white',
    fontSize: 18,
  },
  userNStyle: {
    color: 'white',
    fontSize: 14,
  },
  buttonStyle: {
    fontSize: 20,
    color: '#a9071a',
    textAlign: 'center',
    padding: 2,
  },
  container1: {
    flexDirection: 'row',
    // borderWidth: 1,
    height: heightPercentageToDP('10'),
    position: 'absolute',
    top: heightPercentageToDP('2.5'),
    // left: widthPercentageToDP('1'),
    width: widthPercentageToDP('98'),
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  buttonContainer: {
    backgroundColor: '#a9071a',
    width: widthPercentageToDP('50'),
    height: heightPercentageToDP('8'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: heightPercentageToDP('2'),
  },
  buttonText: {
    color: '#ED1C25',
    fontSize: 21,
    padding: 10,
    textAlign: 'center',
  },
  readTextContainer: {
    width: widthPercentageToDP('90'),
    height: heightPercentageToDP('40'),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: heightPercentageToDP('2'),
  },
  readTextContainer: {
    width: widthPercentageToDP('90'),
    height: heightPercentageToDP('10'),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: heightPercentageToDP('2'),
  },
  digitalSign: {
    width: widthPercentageToDP('80'),
    height: heightPercentageToDP('10'),

    color: '#ED1C25',
    backgroundColor: 'white',
    fontSize: 20,
  },
});
