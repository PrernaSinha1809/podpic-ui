import React from 'react';
import {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Alert,
  ScrollView,
  SafeAreaView,
  StatusBar,
  Button,
  TextInput,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/Entypo';
import Icon1 from 'react-native-vector-icons/AntDesign';
import ImagePicker from './components/ImagePicker';
import ScanPicker from './components/ScannerComp';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const WelcomePage = ({route}) => {
  const navigation = useNavigation();

  const [imgArray, setImgArray] = useState([]);
  const [userInfo, setUserInfo] = useState(route.params.userInfo);
  const [readText, setReadText] = useState('');
  const [roles, setRole] = useState([]);
  const [roleName, setRoleName] = useState('');
  const [img1, setImg1] = useState(false);
  const [img2, setImg2] = useState(false);
  const [trackNo, setTrackNo] = useState('');
  const [recipientEmail, setRecipientEmail] = useState('');

  useEffect(() => {
    setRole(userInfo.roles);
    // logout();
  });

  const logout = () => {
    console.log(recipientEmail);
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    var raw = JSON.stringify({
      username: userInfo.username,
    });
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };
    fetch('http://208.82.115.154:8080/api/auth/signout', requestOptions)
      .then(response => response.json())
      .then(result => navigation.navigate('SignIn'))
      .catch(error => console.log('error', error));
  };

  const addContainer = () => {
    console.log(imgArray);
    var axios = require('axios');
    var data = JSON.stringify({
      trackNo: trackNo,
      // logisticsCompany: 'gta Logistics',
      // transporterName: 'WER',
      // notes: 'testing',
      image1: imgArray[1] != null ? imgArray[1] : null,
      image2: imgArray[2] != null ? imgArray[2] : null,
      image3: imgArray[3] != null ? imgArray[3] : null,
      handler: [userInfo.username],
      email: recipientEmail,
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwZjgzYTQ1OWJkMWU5NzMxOWVjY2RmNyIsImlhdCI6MTYyNzAzNjc3OCwiZXhwIjoxNjI3MTIzMTc4fQ.oNWE9O3s5efq_2c7BVKAI92t9MZp-QCZfrveC-m2i9c',
    });
    var config = {
      method: 'post',
      url: 'http://208.82.115.154:8080/api/auth/addContainer',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };
    axios(config)
      .then(function (response) {
        if (
          response.data.message == 'Track No. not scanned properly. Scan Again!'
        ) {
          Alert.alert('Track No. not scanned properly. Scan Again!');
        } else if (response.data.message == 'Container already exists!') {
          Alert.alert('Container already exists!');
        } else {
          sendEmail();
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const sendEmail = () => {
    const images = imgArray.shift();
    AsyncStorage.setItem('Track No', trackNo);
    setRoleName(JSON.stringify(roles[0]));
    if (userInfo.status != 'Closed') {
      if (JSON.stringify(roles[0]) == '"ROLE_SENDER"') {
        var axios = require('axios');
        var data = JSON.stringify({
          email: userInfo.email,
          text: readText,
          imgArr: imgArray,
        });
        var config = {
          method: 'post',
          url: 'http://208.82.115.154:8080/api/auth/sendEmail',
          headers: {
            'Content-Type': 'application/json',
          },
          data: data,
        };
        axios(config)
          .then(function (response) {
            Alert.alert(response.data + ' to the email ' + userInfo.email);
          })
          .then(result =>
            navigation.navigate('PDFViewer', {
              imageInfo: imgArray,
              userInfo: userInfo,
              readText: readText,
              roleName: '"ROLE_SENDER"',
            }),
          )
          .catch(function (error) {
            console.log(error);
          });
      } else if (JSON.stringify(roles[0]) == '"ROLE_TRANSPORTER/SHIPPER"') {
        var axios = require('axios');
        var data = JSON.stringify({
          email: userInfo.email,
          text: readText,
          imgArr: imgArray,
        });
        var config = {
          method: 'post',
          url: 'http://208.82.115.154:8080/api/auth/sendEmail',
          headers: {
            'Content-Type': 'application/json',
          },
          data: data,
        };
        axios(config)
          .then(function (response) {
            Alert.alert(response.data + ' to the email ' + userInfo.email);
          })
          .then(result =>
            navigation.navigate('PDFViewer', {
              imageInfo: imgArray,
              userInfo: userInfo,
              readText: readText,
              roleName: '"ROLE_TRANSPORTER/SHIPPER"',
            }),
          )
          .catch(function (error) {
            console.log(error);
          });
      } else if (JSON.stringify(roles[0]) == '"ROLE_COURIER"') {
        var axios = require('axios');
        var data = JSON.stringify({
          email: userInfo.email,
          text: readText,
          imgArr: imgArray,
        });
        var config = {
          method: 'post',
          url: 'http://208.82.115.154:8080/api/auth/sendEmail',
          headers: {
            'Content-Type': 'application/json',
          },
          data: data,
        };
        axios(config)
          .then(function (response) {
            Alert.alert(response.data + ' to the email ' + userInfo.email);
          })
          .then(result =>
            navigation.navigate('PDFViewer', {
              imageInfo: imgArray,
              userInfo: userInfo,
              readText: readText,
              roleName: '"ROLE_COURIER"',
            }),
          )
          .catch(function (error) {
            console.log(error);
          });
      }
    } else {
      Alert.alert('Container Status Closed!');
    }
  };

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <LinearGradient
        start={{x: 0.0, y: 0}}
        end={{x: 0.5, y: 1.2}}
        locations={[0, 0.5, 5]}
        colors={['white', '#c1bebf', '#abb8c3']}
        style={styles.linearGradient}>
        <View style={styles.container}>
          <View style={styles.container1}>
            <View style={styles.titleBar}>
              {/* <View> */}
              <View style={styles.burgerIconContainer}>
                <TouchableOpacity onPress={() => navigation.openDrawer()}>
                  <Icon name="menu" size={32} color="white" />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: widthPercentageToDP('25'),
                  // borderWidth: 1,
                  height: heightPercentageToDP('10'),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={styles.welcStyle}>Welcome</Text>
              </View>
              <View style={[styles.welcContainerNormal]}>
                <View>
                  <Text style={styles.userNStyle}>{userInfo.username}</Text>
                </View>
                <View>
                  <TouchableOpacity
                    onPress={() => logout()}
                    style={{
                      width: widthPercentageToDP('15'),
                      height: heightPercentageToDP('9'),
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon1 name="logout" size={25} color="white" />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>

          <View>
            <SafeAreaView style={[styles.container]}>
              <ScrollView
                style={styles.scrollView}
                contentContainerStyle={{
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                  paddingVertical: heightPercentageToDP('10'),
                  // borderWidth: 1,
                }}>
                {userInfo.status != 'Closed' ? (
                  <View>
                    <View
                      style={[
                        {
                          width: widthPercentageToDP('98'),
                          justifyContent: 'center',
                          alignItems: 'center',
                          height: heightPercentageToDP('30'),
                          // borderWidth: 1,
                        },
                      ]}>
                      <ImagePicker
                        sendImageInfo={setImgArray}
                        image1={setImg1}
                      />
                    </View>
                    <View
                      style={{
                        width: widthPercentageToDP('98'),
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        height: heightPercentageToDP('40'),
                        // borderWidth: 1,
                      }}>
                      <ScanPicker
                        sendReadText={setReadText}
                        image2={setImg2}
                        // sendScannedInfo={(setTrackNo, setRecipientEmail)}
                        sendTrackNo={setTrackNo}
                        sendRecipientEmail={setRecipientEmail}
                      />
                    </View>
                    <View style={{alignItems: 'center'}}>
                      {img1 == true && img2 == true ? (
                        <TouchableOpacity
                          style={styles.buttonContainer}
                          onPress={() => addContainer()}>
                          <Text
                            style={[
                              styles.userNStyle,
                              {fontSize: 15, textAlign: 'center'},
                            ]}>
                            Send Email
                          </Text>
                        </TouchableOpacity>
                      ) : null}
                    </View>
                  </View>
                ) : null}
              </ScrollView>
            </SafeAreaView>
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};

export default WelcomePage;

const styles = StyleSheet.create({
  linearGradient: {
    width: widthPercentageToDP('100'),
    height: heightPercentageToDP('100'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollView: {
    marginTop: heightPercentageToDP('12'),
    marginHorizontal: widthPercentageToDP('1'),
    height: heightPercentageToDP('80'),
  },
  text: {
    fontSize: 42,
  },
  linearGradientTextInput: {
    height: heightPercentageToDP('10'),
    justifyContent: 'center',
    alignItems: 'center',
    width: widthPercentageToDP('90'),
  },
  titleBar: {
    // borderWidth: 1,
    width: widthPercentageToDP('95'),
    height: heightPercentageToDP('10'),
    backgroundColor: '#a9071a',
    borderRadius: 10,
    justifyContent: 'space-around',
    flexDirection: 'row',
    alignItems: 'center',
    // marginTop: heightPercentageToDP('5')
  },
  burgerIconContainer: {
    justifyContent: 'center',
    width: widthPercentageToDP('10'),
    height: heightPercentageToDP('10'),
    alignItems: 'center',
    // borderWidth: 1
  },
  welcContainerNormal: {
    // borderWidth: 1,
    // borderColor: 'white',
    justifyContent: 'space-between',
    width: widthPercentageToDP('40'),
    height: heightPercentageToDP('10'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  welcStyle: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  userNStyle: {
    color: 'white',
    fontSize: 18,
  },
  buttonStyle: {
    fontSize: 20,
    color: '#a9071a',
    textAlign: 'center',
    padding: 2,
  },
  container1: {
    flexDirection: 'row',
    // borderWidth: 1,
    height: heightPercentageToDP('10'),
    position: 'absolute',
    top: heightPercentageToDP('0'),
    left: widthPercentageToDP('1'),
    width: widthPercentageToDP('98'),
    justifyContent: 'space-evenly',
    alignItems: 'center',
    zIndex: 1,
    marginTop: heightPercentageToDP('3'),
  },
  buttonContainer: {
    backgroundColor: '#a9071a',
    width: widthPercentageToDP('50'),
    height: heightPercentageToDP('10'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#ED1C25',
    fontSize: 21,
    padding: 10,
    textAlign: 'center',
  },
});
