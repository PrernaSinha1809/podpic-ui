import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import axios from 'axios';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const SignIn = ({route}) => {
  const navigation = useNavigation();
  const [isVisible, setIsVisible] = useState(true);
  const [value, setValue] = useState('');
  const [formData, setFormData] = useState({
    username: '',
    password: '',
  });

  setTimeout(() => {
    setIsVisible(false);
  }, 3000);

  const isFocused = useIsFocused();

  useEffect(() => {
    // Update the document title using the browser API
    setValue(route.params.paramKey);
  });

  useEffect(() => {
    setFormData({...formData, username: ''});
    setFormData({...formData, password: ''});
  }, [isFocused]);

  const onLogin = () => {
    if (formData.username == '') {
      alert('Enter username');
    } else if (formData.password == '') {
      alert('Enter password');
    } else {
      axios
        .post('http://208.82.115.154:8080/api/auth/signin', formData)
        .then(function (response) {
          // handle success
          console.log(JSON.stringify(response.data));
          AsyncStorage.setItem('User Data', JSON.stringify(response.data));
          navigation.navigate('Welcome Page', {
            screen: 'Welcome Page',
            params: {
              userInfo: response.data,
              role: value,
            },
          });
        })
        .catch(function (error) {
          // handle error
          alert('No such user found');
        });
    }
  };

  return (
    <View style={styles.container}>
      <LinearGradient
        start={{x: 0.0, y: 0}}
        end={{x: 0.5, y: 1.2}}
        locations={[0, 0.5, 5]}
        colors={['white', '#c1bebf', '#abb8c3']}
        style={styles.linearGradient}>
        <View>
          <View>
            <View style={styles.logoContainer}>
              <Image
                source={require('../assets/images/L1.png')}
                style={styles.logo}
              />
            </View>
            <View>
              <View style={styles.titleContainer}>
                <Text style={styles.logoStyle}>Login</Text>
              </View>
              <View style={styles.formContainer}>
                <View
                  style={{
                    borderWidth: 2,
                    borderColor: '#d27984',
                    marginVertical: 10,
                  }}>
                  <TextInput
                    placeholder="Username"
                    placeholderTextColor="#cccccc"
                    style={styles.textInput}
                    onChangeText={text =>
                      setFormData({...formData, username: text})
                    }
                    value={formData.username}
                  />
                </View>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('ForgotUserPwd', {
                      paramKey: 'username',
                      // choice: 'username',
                    })
                  }
                  style={{
                    alignItems: 'flex-end',
                  }}>
                  <Text style={styles.touchabletext}>Forgot Username?</Text>
                </TouchableOpacity>
                <View
                  style={{
                    borderWidth: 2,
                    borderColor: '#d27984',
                    marginVertical: 10,
                  }}>
                  <TextInput
                    placeholder="Password"
                    placeholderTextColor="#cccccc"
                    style={styles.textInput}
                    secureTextEntry={true}
                    onChangeText={text =>
                      setFormData({...formData, password: text})
                    }
                    value={formData.password}
                  />
                </View>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('ForgotUserPwd', {
                      paramKey: 'password',
                      // choice: 'password',
                    })
                  }
                  style={{
                    // borderWidth: 1,
                    alignItems: 'flex-end',
                    // marginTop: 10,
                  }}>
                  <Text style={styles.touchabletext}>Forgot Password?</Text>
                </TouchableOpacity>
              </View>
              <View style={{alignItems: 'center'}}>
                <TouchableOpacity
                  style={{
                    elevation: 10,
                    marginTop: heightPercentageToDP('2'),
                    borderWidth: 2,
                    borderColor: '#d27984',
                    backgroundColor: '#a9071a',
                  }}
                  onPress={() => onLogin()}>
                  <View
                    style={[
                      styles.linearGradientTextInput,
                      {width: widthPercentageToDP('40')},
                    ]}>
                    <Text style={styles.buttonStyle}>Login</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  alignItems: 'center',
                  height: heightPercentageToDP('10'),
                  justifyContent: 'flex-end',
                  // paddingTop: heightPercentageToDP('5'),
                  // borderWidth: 1
                }}>
                <Text style={[styles.linkStyle, {color: '#4d4e4f'}]}>
                  Don't have an account?
                </Text>
                <TouchableOpacity
                  onPress={
                    () =>
                      navigation.navigate('SignUp', {
                        paramKey: value,
                      })
                    // console.log("snaksn", value)
                  }>
                  <Text style={styles.linkStyle}>Sign Up</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};

export default SignIn;

const styles = StyleSheet.create({
  container: {
    // flex: 1
  },
  linearGradient: {
    width: widthPercentageToDP('100'),
    height: heightPercentageToDP('100'),
    justifyContent: 'center',
    alignItems: 'center',
    // flex: 1
  },
  logo: {},
  titleContainer: {
    alignItems: 'center',
    height: heightPercentageToDP('12'),
    justifyContent: 'center',
  },
  logoStyle: {
    fontSize: 30,
    color: '#a9071a',
  },
  formContainer: {
    width: widthPercentageToDP('65'),
    height: heightPercentageToDP('20'),
    justifyContent: 'space-evenly',
  },
  logoContainer: {
    // borderWidth: 1,
    alignItems: 'center',
    position: 'absolute',
    bottom: heightPercentageToDP('52'),
    right: widthPercentageToDP('19'),
  },
  textInput: {
    // borderWidth: 1
    backgroundColor: 'white',
    width: widthPercentageToDP('64'),

    fontSize: 18,
    color: '#ED1C25',
    // height: heightPercentageToDP('2')
  },
  linearGradientTextInput: {
    height: heightPercentageToDP('8'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonStyle: {
    fontSize: 20,
    color: 'white',
  },
  ActivityIndicatorStyle: {
    position: 'absolute',
    top: heightPercentageToDP('30'),
    right: widthPercentageToDP('10'),
  },
  linkStyle: {
    color: '#a9071a',
    fontSize: 15,
  },
  touchabletext: {
    color: '#a9071a',
  },
});
