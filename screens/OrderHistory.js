import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import FlatListContainer from '../screens/components/FlatListContainer';
import Icon from 'react-native-vector-icons/AntDesign';

const OrderHistory = ({navigation}) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.titleContainer}>
        <TouchableOpacity
          style={styles.backIconButton}
          onPress={() => navigation.goBack()}>
          <Icon name="back" size={25} color="#a9071a" />
        </TouchableOpacity>
        <Text style={styles.titleStyle}>Orders Handled</Text>
      </View>
      <View style={{flex: 7, backgroundColor: '#C4C5C5'}}>
        <FlatListContainer />
      </View>
    </View>
  );
};
export default OrderHistory;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
  },
  titleStyle: {
    fontSize: 25,
    fontWeight: 'bold',
  },
});
