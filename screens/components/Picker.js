import * as React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {RadioButton} from 'react-native-paper';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const MyComponent = props => {
  const [checked, setChecked] = React.useState('sender');

  return (
    <View style={styles.compContainer}>
      <View style={styles.buttonContainer}>
        <RadioButton
          value="sender"
          status={checked === 'sender' ? 'checked' : 'unchecked'}
          onPress={() => {
            setChecked('sender'), props.sendValue('sender');
          }}
          uncheckedColor="#a9071a"
          color="#a9071a"
        />
        <Text style={styles.label}>Sender</Text>
      </View>
      <View style={styles.buttonContainer}>
        <RadioButton
          value="transporter/shipper"
          status={checked === 'transporter/shipper' ? 'checked' : 'unchecked'}
          onPress={() => {
            setChecked('transporter/shipper'),
              props.sendValue('transporter/shipper');
          }}
          uncheckedColor="#a9071a"
          color="#a9071a"
        />
        <Text style={styles.label}>Transporter/Shipper</Text>
      </View>
      <View style={styles.buttonContainer}>
        <RadioButton
          value="courier"
          status={checked === 'courier' ? 'checked' : 'unchecked'}
          onPress={() => {
            setChecked('courier'), props.sendValue('courier');
          }}
          uncheckedColor="#a9071a"
          color="#a9071a"
        />
        <Text style={styles.label}>Courier</Text>
      </View>
    </View>
  );
};

export default MyComponent;

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: widthPercentageToDP('60'),
  },
  label: {
    fontWeight: 'bold',
    fontSize: 20,
  },
});
