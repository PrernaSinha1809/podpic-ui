import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';

// import Icon1 from 'react-native-vector-icons/Entypo'

const HelloWorldApp = props => {
  const [imagesState, setImagesState] = useState([0]);
  const [resImg, setResImg] = useState([]);

  useEffect(() => {
    sendingProps();
  });

  const openImagePicker = option => {
    ImagePicker.openCamera({
      compressImageQuality: 0.8,
    })
      .then(images => {
        const arr = [];
        arr.push(images);
        setImagesState(arr);
        // uploadImage(images);
      })
      .catch(e => console.log(e));
  };
  const openImagePicker2 = option => {
    ImagePicker.openCamera({
      compressImageQuality: 0.8,
    })
      .then(images => {
        setImagesState(temp => [...temp, images]);
        // uploadImage(images);
      })
      .catch(e => console.log(e));
  };
  const deleteImages = e => {
    // console.log(resImg);
    ImagePicker.clean()
      .then(() => {
        setImagesState(0);
      })
      .catch(e => {
        alert(e);
      });
  };

  const uploadImage = images => {
    const createFormData = (photo, body) => {
      const data = new FormData();
      const timeStamp = new Date();
      data.append('photo', {
        name: 'photo' + timeStamp.getTime() + '.jpeg',
        type: photo.mime,
        uri:
          Platform.OS === 'android'
            ? photo.path
            : photo.path.replace('file://', ''),
      });
      Object.keys(body).forEach(key => {
        data.append(key, body[key]);
      });
      return data;
    };
    setResImg([null]);
    for (var i = 0; i < imagesState.length; i++) {
      fetch('http://208.82.115.154:8080/api/auth/uploadImg', {
        method: 'POST',
        body: createFormData(imagesState[i], {userId: i}),
      })
        .then(response => response.json())
        .then(response => {
          setResImg(temp => [...temp, response.message]);
          alert('Upload success!');
          sendingProps();
        })
        .catch(error => {
          console.log('upload error', error);
          alert('Upload failed!');
        });
    }
  };

  const sendingProps = () => {
    props.image1(true);
    props.sendImageInfo(resImg);
    AsyncStorage.setItem('imgArray', JSON.stringify(resImg));
  };
  const ImageTag = () => {
    return imagesState.map((value, index) => {
      return (
        <View key={index}>
          <View>
            <View></View>
            <View>
              <Image
                source={{uri: value.path}}
                style={{
                  width: 100,
                  height: 100,
                  // position: 'absolute',
                  marginHorizontal: widthPercentageToDP('3'),
                }}
              />
            </View>
          </View>
        </View>
      );
    });
  };
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      {imagesState != 0 ? (
        <View
          style={{
            marginHorizontal: 13,
            marginVertical: 5,
          }}>
          <ScrollView
            horizontal={true}
            contentContainerStyle={{
              alignItems: 'center',
            }}>
            {ImageTag()}
            <View>
              {imagesState.length < 3 ? (
                <TouchableOpacity onPress={() => openImagePicker2()}>
                  <Icon name="add-a-photo" size={25} />
                </TouchableOpacity>
              ) : null}
            </View>
          </ScrollView>
        </View>
      ) : null}

      {imagesState == 0 ? (
        <TouchableOpacity
          style={{
            borderWidth: 1,
            width: widthPercentageToDP('50'),
            height: heightPercentageToDP('10'),
            backgroundColor: 'white',
            elevation: 10,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: '#d27984',
          }}
          onPress={() => openImagePicker()}>
          <Text
            style={{
              fontSize: 20,
              color: '#a9071a',
              textAlign: 'center',
            }}>
            Capture Photo
          </Text>
        </TouchableOpacity>
      ) : null}
      {imagesState != 0 ? (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            width: '80%',
            // borderWidth: 1,
          }}>
          <TouchableOpacity
            style={{
              borderWidth: 1,
              width: widthPercentageToDP('30'),
              height: heightPercentageToDP('7'),
              backgroundColor: 'white',
              elevation: 10,
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: '#d27984',
            }}
            onPress={() => uploadImage()}>
            <Text
              style={{
                fontSize: 17,
                color: '#a9071a',
                textAlign: 'center',
              }}>
              Upload
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              borderWidth: 1,
              width: widthPercentageToDP('30'),
              height: heightPercentageToDP('7'),
              backgroundColor: 'white',
              elevation: 10,
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: '#d27984',
            }}
            onPress={() => deleteImages()}>
            <Text
              style={{
                fontSize: 17,
                color: '#a9071a',
                textAlign: 'center',
              }}>
              Delete
            </Text>
          </TouchableOpacity>
        </View>
      ) : null}
    </View>
  );
};
export default HelloWorldApp;
