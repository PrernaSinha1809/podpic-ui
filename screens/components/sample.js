import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

import axios from 'axios';

const ForgotUserPwd = ({navigation, route}) => {
  const [show, setShow] = useState(route.params.paramKey);

  const [formData, setFormData] = useState({
    email: '',
    nchoice: route.params.paramKey,
    password: '',
  });

  const sendMail = () => {
    axios
      .post('http://208.82.115.154:8080/api/auth/rcrUsername', formData)
      .then(response => {
        // handle success
        console.log('my data', JSON.stringify(response.data));
        Alert.alert(JSON.stringify(response.data));
      })
      .catch(error => {
        // handle error
        Alert.alert('Something Went Wrong!');
      });
    navigation.navigate('SignIn');
  };
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <LinearGradient
        start={{x: 0.0, y: 0}}
        end={{x: 0.5, y: 1.2}}
        locations={[0, 0.5, 5]}
        colors={['white', '#c1bebf', '#abb8c3']}
        style={styles.linearGradient}>
        <View style={styles.formContainer}>
          <View
            style={{
              borderWidth: 2,
              borderColor: '#d27984',
              marginVertical: 10,
            }}>
            {show == 'username' ? (
              <TextInput
                placeholder="Enter email to retrieve username"
                style={styles.textInput}
                onChangeText={text =>
                  setFormData({
                    ...formData,
                    email: text,
                  })
                }
              />
            ) : show == 'password' ? (
              <View>
                <TextInput
                  placeholder="Enter email to retrieve password"
                  style={[styles.textInput, {marginVertical: 5}]}
                  onChangeText={text => setFormData({...formData, email: text})}
                />
                <TextInput
                  placeholder="Enter new password"
                  style={styles.textInput}
                  onChangeText={text =>
                    setFormData({...formData, password: text})
                  }
                />
              </View>
            ) : (
              <TextInput
                placeholder="Enter email to retrieve username"
                style={styles.textInput}
                onChangeText={text =>
                  setFormData({...formData, email: text, nchoice: 'username'})
                }
              />
            )}
          </View>
          <View style={{alignItems: 'center'}}>
            <TouchableOpacity
              style={{
                elevation: 10,
                marginTop: heightPercentageToDP('2'),
                borderWidth: 2,
                borderColor: '#d27984',
              }}
              onPress={() => sendMail()}>
              <View
                style={[
                  styles.linearGradientTextInput,
                  {
                    width: widthPercentageToDP('40'),
                    backgroundColor: '#a9071a',
                  },
                ]}>
                {show == 'username' ? (
                  <Text style={styles.buttonStyle}>Send Email</Text>
                ) : (
                  <Text style={styles.buttonStyle}>Set Password</Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};
export default ForgotUserPwd;

const styles = StyleSheet.create({
  linearGradient: {
    width: widthPercentageToDP('100'),
    height: heightPercentageToDP('100'),
    justifyContent: 'center',
    alignItems: 'center',
    // flex: 1
  },
  formContainer: {
    width: widthPercentageToDP('98'),
    height: heightPercentageToDP('20'),
    // justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    // borderWidth: 1,
    backgroundColor: 'white',
    width: widthPercentageToDP('80'),
    color: '#a9071a',
    fontSize: 18,
    justifyContent: 'center',
    // height: heightPercentageToDP('2')
  },
  linearGradientTextInput: {
    height: heightPercentageToDP('8'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonStyle: {
    fontSize: 20,
    color: 'white',
  },
});
