import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
  ActivityIndicator,
  TextInput,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import Ocr from 'react-native-tesseract-ocr';

const tessOptions = {
  whitelist: null,
  blacklist: null,
};

const HelloWorldApp = props => {
  const [imagesState, setImagesState] = useState([0]);
  const [info, setInfo] = useState('');
  const [resStatus, setResStatus] = useState(false);
  const [showTextInput, setShowTextInput] = useState(false);
  const [recipientEmail, setRecipientEmail] = useState('');

  useEffect(() => {});

  const uploadImage = images => {
    const createFormData = (photo, body) => {
      const data = new FormData();
      const timeStamp = new Date();
      data.append('photo', {
        name: 'photoScan' + timeStamp.getTime() + '.jpeg',
        type: photo.mime,
        uri:
          Platform.OS === 'android'
            ? photo.path
            : photo.path.replace('file://', ''),
      });
      console.log('0-0-0', data);
      Object.keys(body).forEach(key => {
        data.append(key, body[key]);
      });

      return data;
    };

    fetch('http://208.82.115.154:8080/api/auth/uploadSingleImg', {
      method: 'POST',
      body: createFormData(images, {userId: '12'}),
    })
      .then(response => response.json())
      .then(response => {
        console.log('-------kk------------', response.message);
        console.log('-------kk------------', response.message.includes('SR#'));
        console.log('-------kk------------', response.message.includes('.com'));
        if (
          response.message.includes('SR#') == false ||
          response.message.includes('.com') == false
        ) {
          Alert.alert('Could Not Scan Information. Please Enter Manually.');
          setShowTextInput(true);
          props.sendReadText('Could Not Scan Information.');
        } else {
          setShowTextInput(false);
        }
        setResStatus(true);
        const temp = response.message;
        let TrackNo = '';
        let index = '';
        let recipientEmail = '';
        if (temp.includes('SR#') == true) {
          index = temp.search('SR#');
          TrackNo = temp.slice(index, index + 17);
          // sendProps();
        }

        if (temp.includes('.com')) {
          index = temp.search('.com');
          recipientEmail = temp.slice(index - 15, index + 4);
          console.log('recipientEmail', recipientEmail);
        }
        props.sendReadText(response.message);
        props.sendTrackNo(TrackNo);
        props.sendRecipientEmail(recipientEmail);
        // alert('Upload success!');
        props.image2(true);
      })
      .catch(error => {
        console.log('upload error', error);
        alert('Upload failed!');
      });
  };

  const openImagePicker = option => {
    ImagePicker.openCamera({
      cropping: true,
      compressImageQuality: 0.09,
      maxFiles: 1,
    })
      .then(images => {
        const arr = [];
        arr.push(images);
        setImagesState(arr);
        uploadImage(images);
      })
      .catch(e => console.log(e));
  };
  const deleteImages = () => {
    ImagePicker.clean()
      .then(() => {
        setImagesState(0);
        setResStatus(false);
      })
      .catch(e => {
        alert(e);
      });
  };
  const ImageTag = () => {
    return imagesState.map((value, index) => {
      return (
        <View key={index}>
          <View>
            <Image
              source={{uri: value.path}}
              style={{
                width: 100,
                height: 100,
                marginHorizontal: widthPercentageToDP('3'),
              }}
            />
          </View>
        </View>
      );
    });
  };

  const checkInput = () => {
    if (
      recipientEmail.includes('gmail.com') == true ||
      recipientEmail.includes('hotmail.com') == true ||
      recipientEmail.includes('yahoo.com') == true ||
      recipientEmail.includes('podpic.com') == true
    ) {
      props.sendRecipientEmail(recipientEmail);
      props.image2(true);
      setShowTextInput(false);
    } else {
      Alert.alert('Enter valid email address');
    }
  };

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      {imagesState != 0 ? (
        <View
          style={{
            marginHorizontal: 13,
            marginVertical: 5,
          }}>
          {resStatus == false ? (
            <ActivityIndicator size="small" color="#a9071a" />
          ) : (
            <View
              horizontal={true}
              style={
                {
                  // borderWidth: 1,
                }
              }>
              {showTextInput == false ? (
                <View>{ImageTag()}</View>
              ) : (
                <View>
                  <View
                    style={{
                      borderWidth: 1,
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderColor: '#a9071a',
                      backgroundColor: 'white',
                    }}>
                    <Text
                      style={{
                        fontSize: 15,
                        marginHorizontal: 5,
                        color: '#a9071a',
                      }}>
                      SR#
                    </Text>
                    <TextInput
                      placeholder="Enter Tracking Number of the Container"
                      placeholderTextColor="grey"
                      maxLength={13}
                      keyboardType="numeric"
                      onChangeText={TrackNo => {
                        TrackNo.includes('SR#') == true ||
                        TrackNo.includes('Sr#') ||
                        TrackNo.includes('sr#')
                          ? Alert.alert('Just enter the number')
                          : props.sendTrackNo('SR#' + TrackNo);
                      }}
                      style={{
                        color: '#a9071a',
                        backgroundColor: 'white',
                      }}
                    />
                  </View>
                  <View>
                    <TextInput
                      placeholder="Enter Recipient Email on the Container"
                      placeholderTextColor="grey"
                      keyboardType="email-address"
                      onChangeText={recipientEmail =>
                        setRecipientEmail(recipientEmail)
                      }
                      style={{
                        borderWidth: 1,
                        color: '#a9071a',
                        borderColor: '#a9071a',
                        backgroundColor: 'white',
                      }}
                    />
                  </View>
                </View>
              )}
            </View>
          )}
        </View>
      ) : null}

      {imagesState == 0 ? (
        <TouchableOpacity
          style={{
            borderWidth: 1,
            width: widthPercentageToDP('50'),
            height: heightPercentageToDP('10'),
            backgroundColor: 'white',
            elevation: 10,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: '#d27984',
          }}
          onPress={() => openImagePicker()}>
          <Text
            style={{
              fontSize: 20,
              color: '#a9071a',
              textAlign: 'center',
            }}>
            Scan Shipping Label
          </Text>
        </TouchableOpacity>
      ) : null}

      {imagesState != 0 ? (
        <View>
          {showTextInput == true ? (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                width: '80%',
                marginTop: 10,
                // borderWidth: 1,
              }}>
              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  width: widthPercentageToDP('30'),
                  height: heightPercentageToDP('7'),
                  backgroundColor: 'white',
                  elevation: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderColor: '#d27984',
                }}
                onPress={() => deleteImages()}>
                <Text
                  style={{
                    fontSize: 17,
                    color: '#a9071a',
                    textAlign: 'center',
                  }}>
                  Scan Again
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  width: widthPercentageToDP('30'),
                  height: heightPercentageToDP('7'),
                  backgroundColor: 'white',
                  elevation: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderColor: '#d27984',
                }}
                onPress={() => checkInput()}>
                <Text
                  style={{
                    fontSize: 17,
                    color: '#a9071a',
                    textAlign: 'center',
                  }}>
                  Done
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <TouchableOpacity
              style={{
                borderWidth: 1,
                width: widthPercentageToDP('30'),
                height: heightPercentageToDP('7'),
                backgroundColor: 'white',
                elevation: 10,
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#d27984',
              }}
              onPress={() => deleteImages()}>
              <Text
                style={{
                  fontSize: 20,
                  color: '#a9071a',
                  textAlign: 'center',
                }}>
                Delete
              </Text>
            </TouchableOpacity>
          )}
        </View>
      ) : null}
    </View>
  );
};
export default HelloWorldApp;
