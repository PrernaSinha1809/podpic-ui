import React, {useState, useEffect} from 'react';
import {
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/core';

const App = props => {
  const navigation = useNavigation();
  const [selectedId, setSelectedId] = useState(null);
  const [userInfo, setUserInfo] = useState();
  const [containerInfo, setContainerInfo] = useState();
  const [flag, setFlag] = useState(true);
  const [isloaded, setLoaded] = useState(false);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    AsyncStorage.getItem('User Data').then(value => {
      setUserInfo(JSON.parse(value));
    });
  };

  if (userInfo && isloaded == false) {
    var axios = require('axios');
    var data = JSON.stringify({
      username: `${userInfo.username}`,
    });

    var config = {
      method: 'post',
      url: 'http://208.82.115.154:8080/api/auth/getAllContainers',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        if (response.data == null) {
          setFlag(true);
          setLoaded(true);
        } else {
          setContainerInfo(response.data);
          setFlag(false);
          setLoaded(true);
        }
      })
      .catch(function (error) {
        console.log(error);
        setFlag(true);
      });
  }

  const getInformation = item => {
    if (item.status == 'In-Motion') {
      AsyncStorage.setItem('Track No', item.trackNo);
      navigation.navigate('TransportImages', {
        userInfo: userInfo,
        roleName: JSON.stringify(userInfo.roles[0]),
      });
    } else if (item.status == 'In-Progress') {
      AsyncStorage.setItem('Track No', item.trackNo);
      navigation.navigate('StatusHandle', {
        userInfo: userInfo,
        roleName: JSON.stringify(userInfo.roles[0]),
      });
    } else if (item.status == 'Closed') {
      Alert.alert('This container has been closed!');
    }
  };
  const Item = ({item, onPress, backgroundColor, textColor}) => (
    <TouchableOpacity
      onPress={() => getInformation(item)}
      style={[styles.item, backgroundColor]}>
      <Text style={[styles.title, textColor]}>Track No: {item.trackNo}</Text>
      <Text style={[styles.title, textColor]}>
        Logistics Company: {item.logisticsCompany}
      </Text>
      <Text style={[styles.title, textColor]}>Notes: {item.notes}</Text>
      <Text style={[styles.title, textColor]}>Status: {item.status}</Text>
      <Text style={[styles.title, textColor]}>Signature: {item.signature}</Text>
    </TouchableOpacity>
  );

  const renderItem = ({item}) => {
    const backgroundColor = item._id === selectedId ? 'white' : 'white';
    const color = item._id === selectedId ? 'black' : 'black';

    return (
      <Item
        item={item}
        onPress={() => setSelectedId(item._id)}
        backgroundColor={{backgroundColor}}
        textColor={{color}}
      />
    );
  };

  if (containerInfo) {
    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          data={containerInfo}
          renderItem={renderItem}
          keyExtractor={item => item._id}
          extraData={selectedId}
        />
      </SafeAreaView>
    );
  } else {
    return (
      <View
        style={{
          flex: 2,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {flag == true ? (
          <View>
            <ActivityIndicator size="small" color="#a9071a" />
          </View>
        ) : null}
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 20,
  },
});

export default App;
