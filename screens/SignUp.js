import React from 'react';
import {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Checkbox} from 'react-native-paper';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import axios from 'axios';

const formArray = [
  {label: 'Username', placeholder: 'Enter here'},
  {label: 'Name', placeholder: 'Enter here'},
  {label: 'Mobile No', placeholder: 'Enter here'},
  {label: 'Email', placeholder: 'Enter here'},
  {label: 'Role', placeholder: 'Enter here'},
  {label: 'Password', placeholder: '**********'},
  {label: 'Confirm Password', placeholder: '**********'},
];

const SignUp = ({navigation, route}) => {
  const [checked, setChecked] = React.useState(false);
  const [value, setValue] = useState();
  const [formData, setFormData] = useState({
    username: '',
    name: '',
    mobileNo: '',
    email: '',
    roles: [route.params.paramKey],
    password: '',
    signature: 'No Signature',
    status: 'In-Progress',
  });

  const SignUpApiCall = () => {
    console.log(formData);
    if (checked == false) {
      Alert.alert('Please agree to the Terms and Conditions');
    } else {
      axios
        .post('http://208.82.115.154:8080/api/auth/signup', formData)
        .then(function (response) {
          // handle success
          console.log(JSON.stringify(response.data));
          navigation.navigate('SignIn');
        })
        .catch(function (error) {
          // handle error
          alert(error);
        });
    }
  };

  const formComponent = () => {
    return formArray.map((info, key) => {
      return (
        <View
          key={key}
          style={{borderWidth: 2, borderColor: '#d27984', marginVertical: 2}}>
          <View style={styles.cellContainer}>
            <View
              style={{
                // borderWidth: 1,
                width: widthPercentageToDP('25'),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.labelStyle}>{info.label}</Text>
            </View>
            <View
              style={{
                // borderWidth: 1,
                width: widthPercentageToDP('64'),
                justifyContent: 'center',
                // alignItems: 'center'
              }}>
              {info.label == 'Password' || info.label == 'Confirm Password' ? (
                <TextInput
                  placeholder={info.placeholder}
                  style={[styles.labelStyle]}
                  secureTextEntry={true}
                  onChangeText={text =>
                    setFormData({...formData, password: text})
                  }
                  // style={{ borderWidth: 1 }}
                />
              ) : info.label == 'Role' ? (
                <View>
                  <Text style={[styles.labelStyle]}>{formData.roles}</Text>
                </View>
              ) : info.label == 'Username' ? (
                <TextInput
                  placeholder={info.placeholder}
                  style={[styles.labelStyle]}
                  onChangeText={text =>
                    setFormData({
                      ...formData,
                      username: text.split(' ').join(''),
                    })
                  }
                  onKeyPress={({nativeEvent}) =>
                    nativeEvent.key == ' ' ? Keyboard.dismiss() : null
                  }
                  value={formData.username}
                />
              ) : info.label == 'Name' ? (
                <TextInput
                  placeholder={info.placeholder}
                  style={[styles.labelStyle]}
                  onChangeText={text => setFormData({...formData, name: text})}
                  // style={{ borderWidth: 1 }}
                />
              ) : info.label == 'Email' ? (
                <TextInput
                  placeholder={info.placeholder}
                  style={[styles.labelStyle]}
                  keyboardType="email-address"
                  onChangeText={text => setFormData({...formData, email: text})}
                  // style={{ borderWidth: 1 }}
                />
              ) : info.label == 'Mobile No' ? (
                <TextInput
                  placeholder={info.placeholder}
                  keyboardType="number-pad"
                  maxLength={10}
                  style={[styles.labelStyle]}
                  onChangeText={text =>
                    setFormData({...formData, mobileNo: text})
                  }
                  // style={{ borderWidth: 1 }}
                />
              ) : null}
            </View>
          </View>
        </View>
      );
    });
  };
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <ScrollView>
        <View>
          <LinearGradient
            start={{x: 0.0, y: 0}}
            end={{x: 0.5, y: 1.2}}
            locations={[0, 0.5, 5]}
            colors={['white', '#c1bebf', '#abb8c3']}
            style={styles.linearGradient}>
            {/* style={{ flex: 1, justifyContent: "center", alignItems: "center" }} */}
            <ScrollView>
              <View>
                <View style={styles.titleContainer}>
                  <Text style={styles.logoStyle}>Sign Up</Text>
                </View>
                <View style={{}}>{formComponent()}</View>
                <View
                  style={{
                    // borderWidth: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                    height: heightPercentageToDP('10'),
                    width: widthPercentageToDP('90'),
                  }}>
                  <Checkbox
                    status={checked ? 'checked' : 'unchecked'}
                    onPress={() => {
                      setChecked(!checked);
                    }}
                    uncheckedColor="white"
                    color="white"
                  />
                  <Text style={styles.termsStyle}>
                    By Signing Up, I agree to the Terms and Conditions
                  </Text>
                </View>
                <View style={{alignItems: 'center'}}>
                  <TouchableOpacity
                    style={{
                      elevation: 10,
                      marginTop: heightPercentageToDP('2'),
                      borderWidth: 2,
                      borderColor: '#d27984',
                      backgroundColor: '#a9071a',
                    }}
                    onPress={() => SignUpApiCall()}
                    // navigation.navigate('WelcomePage')
                  >
                    <View
                      style={[
                        styles.linearGradientTextInput,
                        {
                          width: widthPercentageToDP('40'),
                          height: heightPercentageToDP('7'),
                        },
                      ]}>
                      <Text style={styles.buttonStyle}>Register</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
            {/* </TouchableWithoutFeedback> */}
          </LinearGradient>
        </View>
      </ScrollView>
    </TouchableWithoutFeedback>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  linearGradient: {
    width: widthPercentageToDP('100'),
    height: heightPercentageToDP('105'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  linearGradientTextInput: {
    height: heightPercentageToDP('10'),
    justifyContent: 'center',
    alignItems: 'center',
    width: widthPercentageToDP('90'),
  },
  cellContainer: {
    // borderWidth: 1,
    width: widthPercentageToDP('89'),
    height: heightPercentageToDP('9'),
    backgroundColor: 'white',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  labelStyle: {
    fontSize: 15,
    textAlign: 'center',
    color: '#a9071a',
  },
  titleContainer: {
    // position: 'absolute',
    // top: heightPercentageToDP('25'),
    alignItems: 'center',
    height: heightPercentageToDP('12'),
    // borderWidth: 1,
    justifyContent: 'center',
  },
  logoStyle: {
    fontSize: 30,
    color: '#a9071a',
  },
  termsStyle: {
    fontSize: 14,
    // borderWidth: 1,
    color: 'black',
    textAlign: 'center',
    textShadowColor: 'white',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 2,
  },
  buttonStyle: {
    fontSize: 20,
    color: 'white',
  },
  container: {
    paddingVertical: heightPercentageToDP('5'),
  },
});
