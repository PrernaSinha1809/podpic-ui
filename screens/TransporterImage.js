import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput,
  Alert,
  ActivityIndicator,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/AntDesign';
import ImagePicker from 'react-native-image-crop-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import Logo from '../assets/images/L1.png';

const TransportImages = ({route}) => {
  const navigation = useNavigation();
  const [userInfo, setUserInfo] = useState({});
  const [roleName, setRoleName] = useState('');
  const [block, setBlock] = useState('block 1');
  const [imagesState, setImagesState] = useState([]);
  const [recipientEmail, setRecipientEmail] = useState('');
  const [trackNo, setTrackNo] = useState('');
  const [res, setRes] = useState([]);

  const [loader, setLoader] = useState(false);
  const [data, setData] = useState({
    // status: 'In-Motion',
    signature: '',
  });

  useEffect(() => {
    setUserInfo(route.params.userInfo);
    setRoleName(route.params.roleName);
    fetchTrackNo();
    getRecipient();
  });

  const getRecipient = () => {
    var axios = require('axios');
    var data = JSON.stringify({
      trackNo: trackNo,
    });

    var config = {
      method: 'post',
      url: 'http://208.82.115.154:8080/api/auth/getRecipient',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(response.data);
        setRecipientEmail(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const fetchTrackNo = () => {
    AsyncStorage.getItem('Track No').then(value => {
      setTrackNo(value);
      // console.log(value);
    });
    AsyncStorage.getItem('imgArray').then(abc => {
      // setTrackNo(value);
      // console.log(abc);
    });
  };

  const logout = () => {
    var axios = require('axios');
    var data = JSON.stringify({
      username: userInfo.username,
    });

    var config = {
      method: 'post',
      url: 'http://208.82.115.154:8080/api/auth/signout',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        navigation.navigate('SignIn');
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const updateSign = () => {
    if (data.signature != '') {
      var myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/json');

      var info = JSON.stringify({
        trackNo: trackNo,
        signature: data.signature,
        status: 'Closed',
      });
      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: info,
        redirect: 'follow',
      };

      fetch('http://208.82.115.154:8080/api/auth/addSign', requestOptions)
        .then(response => response.json())
        .then(result => {
          setLoader(true);
        })
        .catch(error => console.log('error', error));
    } else {
      Alert.alert('Now Add Signature!');
    }
  };

  const sendEmail = () => {
    console.log(res);
    var axios = require('axios');
    var data = JSON.stringify({
      email: recipientEmail,
      flag: '2 block',
      imgArr: [res],
    });
    var config = {
      method: 'post',
      url: 'http://208.82.115.154:8080/api/auth/sendEmail',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };
    axios(config)
      .then(function (response) {
        setLoader(false);
        if (userInfo.roles[0] == 'ROLE_TRANSPORTER/SHIPPER') {
          Alert.alert(response.data + ' to the email ' + recipientEmail);
          setBlock('block 2');
          navigation.navigate('SignIn');
        } else {
          Alert.alert(response.data + ' to the email ' + recipientEmail);
          setBlock('block 2');
          navigation.navigate('SignIn');
        }
      })
      .catch(function (error) {
        console.log(error);
        Alert.alert('Error in sending Email!');
        // navigation.navigate('SignIn');
      });
  };

  const uploadImage = arr => {
    console.log('........I am here', arr);
    if (data) {
      const createFormData = (photo, body) => {
        const data = new FormData();
        const timeStamp = new Date();
        data.append('photo', {
          name: 'finalImage' + timeStamp.getTime() + '.jpeg',
          type: photo.mime,
          uri:
            Platform.OS === 'android'
              ? photo.path
              : photo.path.replace('file://', ''),
        });
        Object.keys(body).forEach(key => {
          data.append(key, body[key]);
        });
        return data;
      };

      for (var i = 0; i < arr.length; i++) {
        fetch('http://208.82.115.154:8080/api/auth/uploadFinalContainer', {
          method: 'POST',
          body: createFormData(arr[i], {userId: i}),
        })
          .then(response => response.json())
          .then(response => {
            console.log('.........................', response);
            alert('Upload success!');
            setRes(response.message);
            updateSign();
          })
          .catch(error => {
            console.log('upload error', error);
            alert('Upload failed!');
          });
      }
    }
  };

  const openImagePicker = () => {
    ImagePicker.openCamera({
      multiple: true,
      compressImageQuality: 0.8,
      maxFiles: 3,
    })
      .then(images => {
        const arr = [];
        arr.push(images);
        setImagesState(arr);
        uploadImage(arr);
      })
      .catch(e => console.log(e));
  };

  const ImageTag = () => {
    return imagesState.map((value, index) => {
      return (
        <View key={index}>
          <View>
            <Image
              source={{uri: value.path}}
              style={{
                width: 100,
                height: 100,
                marginHorizontal: widthPercentageToDP('3'),
              }}
            />
          </View>
        </View>
      );
    });
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <LinearGradient
        start={{x: 0.0, y: 0}}
        end={{x: 0.5, y: 1.2}}
        locations={[0, 0.5, 5]}
        colors={['white', '#c1bebf', '#abb8c3']}
        style={styles.linearGradient}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.container1}>
            <View style={styles.titleBar}>
              {/* <View> */}
              <View style={styles.burgerIconContainer}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <Icon name="back" size={25} color="white" />
                </TouchableOpacity>
              </View>
              <View style={styles.welcContainer}>
                <Text style={styles.welcStyle}>Welcome</Text>
              </View>
              <View style={[styles.welcContainerNormal]}>
                <View>
                  <Text style={styles.userNStyle}>{userInfo.username}</Text>
                </View>
                <TouchableOpacity
                  onPress={() => logout()}
                  style={{
                    // borderWidth: 1,
                    width: widthPercentageToDP('15'),
                    alignItems: 'flex-end',
                  }}>
                  <Icon2 name="logout" size={25} color="white" />
                </TouchableOpacity>
              </View>
            </View>
            <View>
              <TouchableOpacity
                style={styles.buttonContainer}
                onPress={() => sendEmail()}>
                <Text style={[styles.userNStyle]}>Send Email</Text>
              </TouchableOpacity>
            </View>
          </View>
          {roleName == '"ROLE_TRANSPORTER/SHIPPER"' ||
          roleName == '"ROLE_COURIER"' ? (
            <View>
              {loader == false ? (
                <View
                  style={{
                    // flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',

                    height: heightPercentageToDP('70'),
                    width: widthPercentageToDP('90'),
                    // borderColor: 'green',
                  }}>
                  {imagesState != null ? (
                    <View
                      style={{
                        height: heightPercentageToDP('50'),
                        width: widthPercentageToDP('80'),
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <View
                        style={{
                          height: heightPercentageToDP('50'),
                          width: widthPercentageToDP('80'),
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <View
                          style={{
                            height: heightPercentageToDP('50'),
                            width: widthPercentageToDP('80'),
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <View>{ImageTag()}</View>
                          <TextInput
                            placeholder="Courier's Signature"
                            placeholderTextColor="grey"
                            style={{
                              borderWidth: 1,
                              borderColor: '#a9071a',
                              marginVertical: 10,
                              color: '#a9071a',
                              backgroundColor: 'white',
                            }}
                            onChangeText={text =>
                              setData({...data, signature: text})
                            }
                          />
                          {/* {roleName != '"ROLE_COURIER"' ? ( */}
                          <TouchableOpacity
                            style={{
                              borderWidth: 1,
                              width: widthPercentageToDP('50'),
                              height: heightPercentageToDP('10'),
                              backgroundColor: 'white',
                              elevation: 10,
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderColor: '#d27984',
                            }}
                            onPress={() => {
                              openImagePicker();
                            }}>
                            <Text
                              style={{
                                fontSize: 20,
                                color: '#a9071a',
                                textAlign: 'center',
                              }}>
                              Capture Final Image
                            </Text>
                          </TouchableOpacity>
                          {/* ) : null} */}
                        </View>
                      </View>
                    </View>
                  ) : null}
                  <View></View>
                </View>
              ) : (
                <ActivityIndicator size="large" color="#a9071a" />
              )}
            </View>
          ) : null}
        </View>
      </LinearGradient>
    </View>
  );
};
export default TransportImages;

const styles = StyleSheet.create({
  linearGradient: {
    width: widthPercentageToDP('100'),
    height: heightPercentageToDP('100'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    paddingTop: heightPercentageToDP('6'),
  },

  titleBar: {
    // borderWidth: 1,
    width: widthPercentageToDP('81'),
    height: heightPercentageToDP('10'),
    backgroundColor: '#a9071a',
    borderRadius: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  burgerIconContainer: {
    justifyContent: 'center',
    width: widthPercentageToDP('10'),
    alignItems: 'center',
  },
  welcContainerNormal: {
    justifyContent: 'center',
    width: widthPercentageToDP('40'),
    height: heightPercentageToDP('10'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  welcStyle: {
    color: 'white',
    fontSize: 18,
  },
  userNStyle: {
    color: 'white',
    fontSize: 14,
  },
  buttonStyle: {
    fontSize: 20,
    color: '#a9071a',
    textAlign: 'center',
    padding: 2,
  },
  container1: {
    flexDirection: 'row',
    // borderWidth: 1,
    height: heightPercentageToDP('10'),
    position: 'absolute',
    top: heightPercentageToDP('2.5'),
    // left: widthPercentageToDP('1'),
    width: widthPercentageToDP('98'),
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  buttonContainer: {
    backgroundColor: '#a9071a',
    width: widthPercentageToDP('14'),
    height: heightPercentageToDP('10'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#ED1C25',
    fontSize: 21,
    padding: 10,
    textAlign: 'center',
  },
  readTextContainer: {
    borderWidth: 1,
    width: widthPercentageToDP('90'),
    height: heightPercentageToDP('10'),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: heightPercentageToDP('2'),
  },
  digitalSign: {
    width: widthPercentageToDP('80'),
    height: heightPercentageToDP('10'),
    borderWidth: 1,
    color: '#ED1C25',
    backgroundColor: 'white',
    fontSize: 20,
  },
});
