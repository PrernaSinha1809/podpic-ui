import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput,
  Alert,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Entypo';
import Icon1 from 'react-native-vector-icons/FontAwesome5';
import Icon2 from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-async-storage/async-storage';
const PDFViewer = ({route}) => {
  const navigation = useNavigation();
  const [userInfo, setUserInfo] = useState({});
  const [roleName, setRoleName] = useState('');
  const [trackNo, setTrackNo] = useState('');
  const [data, setData] = useState({
    // status: 'In-Progress',
    signature: 'No Signature',
  });

  useEffect(() => {
    setUserInfo(route.params.userInfo);
    setRoleName(route.params.roleName);
    fetchTrackNo();
  });

  const fetchTrackNo = () => {
    AsyncStorage.getItem('Track No').then(value => {
      setTrackNo(value);
    });
  };

  const logout = () => {
    var axios = require('axios');
    var data = JSON.stringify({
      username: userInfo.username,
    });

    var config = {
      method: 'post',
      url: 'http://208.82.115.154:8080/api/auth/signout',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        navigation.navigate('SignIn');
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const updateSign = () => {
    var axios = require('axios');
    if (roleName == '"ROLE_SENDER"') {
      var info = JSON.stringify({
        trackNo: trackNo,
        signature: data.signature,
        status: 'Closed',
      });
    } else if (
      roleName == '"ROLE_TRANSPORTER/SHIPPER"' ||
      roleName == '"ROLE_COURIER"'
    ) {
      var info = JSON.stringify({
        trackNo: trackNo,
        signature: data.signature,
        status: 'In-Motion',
      });
    }

    var config = {
      method: 'post',
      url: 'http://208.82.115.154:8080/api/auth/addSign',
      headers: {
        'Content-Type': 'application/json',
      },
      data: info,
    };

    axios(config)
      .then(function (response) {
        if (
          roleName == '"ROLE_TRANSPORTER/SHIPPER"' ||
          roleName == '"ROLE_COURIER"'
        ) {
          Alert.alert(JSON.stringify(response.data));
          navigation.navigate('SignIn');
        } else if (roleName == '"ROLE_SENDER"') {
          navigation.navigate('SignIn');
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <LinearGradient
        start={{x: 0.0, y: 0}}
        end={{x: 0.5, y: 1.2}}
        locations={[0, 0.5, 5]}
        colors={['white', '#c1bebf', '#abb8c3']}
        style={styles.linearGradient}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.container1}>
            <View style={styles.titleBar}>
              {/* <View> */}
              <View style={styles.burgerIconContainer}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <Icon name="back" size={25} color="white" />
                </TouchableOpacity>
              </View>
              <View style={styles.welcContainer}>
                <Text style={styles.welcStyle}>Welcome</Text>
              </View>
              <View style={[styles.welcContainerNormal]}>
                <View>
                  <Text style={styles.userNStyle}>{userInfo.username}</Text>
                </View>
                <TouchableOpacity
                  onPress={() => logout()}
                  style={{
                    // borderWidth: 1,
                    width: widthPercentageToDP('15'),
                    alignItems: 'flex-end',
                  }}>
                  <Icon2 name="logout" size={25} color="white" />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {roleName == '"ROLE_SENDER"' ? (
            <View>
              <View style={styles.readTextContainer}>
                <ScrollView>
                  <Text style={{fontSize: 20}}>Tracking Number: {trackNo}</Text>
                  <Text style={{fontSize: 20}}>Status: {data.status}</Text>
                  <Text style={{fontSize: 20}}>
                    Signature: {data.signature}
                  </Text>
                </ScrollView>
              </View>

              <View
                style={[
                  styles.readTextContainer,
                  {
                    backgroundColor: null,
                    borderWidth: 0,
                    // flexDirection: 'row',
                  },
                ]}>
                <TextInput
                  style={styles.digitalSign}
                  placeholder="Shipper's Signature"
                  placeholderTextColor="#c1c1c1"
                  onChangeText={text =>
                    setData({...data, signature: text + ' (Shipper) '})
                  }
                />
                <TouchableOpacity
                  onPress={() => updateSign()}
                  style={[
                    styles.digitalSign,
                    {
                      marginVertical: heightPercentageToDP('2'),
                      borderRadius: 10,
                      backgroundColor: '#a9071a',
                      // borderWidth: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    },
                  ]}>
                  <Text style={{fontSize: 20, color: 'white'}}>
                    Update Status: Closed
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : roleName == '"ROLE_TRANSPORTER/SHIPPER"' ||
            roleName == '"ROLE_COURIER"' ? (
            <View>
              <View style={styles.readTextContainer}>
                <ScrollView>
                  <Text style={{fontSize: 20}}>Tracking Number: {trackNo}</Text>
                  <Text style={{fontSize: 20}}>Status: {userInfo.status}</Text>
                  <Text style={{fontSize: 20}}>
                    Signature: {data.signature}
                  </Text>
                </ScrollView>
              </View>
              <View style={{alignItems: 'center'}}>
                <View
                  style={[
                    styles.readTextContainer,
                    {
                      backgroundColor: null,
                      borderWidth: 0,
                      flexDirection: 'row',
                    },
                  ]}>
                  <TextInput
                    style={styles.digitalSign}
                    placeholder="Sender's Signature"
                    placeholderTextColor="#cccccc"
                    onChangeText={text => setData({...data, signature: text})}
                  />
                </View>
                <View>
                  <TouchableOpacity
                    style={[
                      styles.digitalSign,
                      {
                        marginVertical: heightPercentageToDP('2'),
                        borderRadius: 10,
                        backgroundColor: '#a9071a',
                        // borderWidth: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      },
                    ]}
                    onPress={() => updateSign()}>
                    <Text style={{fontSize: 20, color: 'white'}}>
                      Update Status: In-Motion
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          ) : (
            <View>
              <View style={styles.readTextContainer}>
                <ScrollView>
                  <Text style={{fontSize: 20}}>Tracking Number: {trackNo}</Text>
                  <TouchableOpacity
                    onPress={() => {
                      setData({...data, status: 'In-Motion'});
                    }}>
                    {userInfo.status == 'In-Progress' ? (
                      <Text style={{fontSize: 20, color: 'blue'}}>
                        Status: {data.status}
                      </Text>
                    ) : (
                      <Text style={{fontSize: 20, color: 'green'}}>
                        Status: {userInfo.status}
                      </Text>
                    )}
                  </TouchableOpacity>
                </ScrollView>
              </View>
              {/* {userInfo.signature == 'No signature' ? (
                <View
                  style={[
                    styles.readTextContainer,
                    {
                      backgroundColor: null,
                      borderWidth: 0,
                      flexDirection: 'row',
                    },
                  ]}>
                  <TextInput
                    style={styles.digitalSign}
                    placeholder="Sender's Signature"
                    placeholderTextColor="#cccccc"
                    onChangeText={text =>
                      setData({...data, signature: text + ' (Sender)'})
                    }
                  />
                  <TouchableOpacity
                    onPress={() => updateSign()}
                    style={[
                      styles.digitalSign,
                      {
                        width: widthPercentageToDP('10'),
                        borderWidth: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      },
                    ]}>
                    <Text style={{fontSize: 20, color: '#ED1C25'}}>OK</Text>
                  </TouchableOpacity>
                </View>
              ) : (
                <View
                  style={[
                    styles.readTextContainer,
                    {
                      backgroundColor: null,
                      borderWidth: 0,
                      flexDirection: 'row',
                    },
                  ]}>
                  <Text
                    style={[
                      styles.digitalSign,
                      {
                        width: widthPercentageToDP('90'),
                        textAlign: 'center',
                      },
                    ]}>
                    Signed By {userInfo.signature}
                  </Text>
                </View>
              )} */}
            </View>
          )}
        </View>
      </LinearGradient>
    </View>
  );
};
export default PDFViewer;

const styles = StyleSheet.create({
  linearGradient: {
    width: widthPercentageToDP('100'),
    height: heightPercentageToDP('100'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    paddingTop: heightPercentageToDP('6'),
  },
  scrollView: {
    marginHorizontal: widthPercentageToDP('1'),
  },
  text: {
    fontSize: 42,
  },
  linearGradientTextInput: {
    height: heightPercentageToDP('10'),
    justifyContent: 'center',
    alignItems: 'center',
    width: widthPercentageToDP('90'),
  },
  titleBar: {
    // borderWidth: 1,
    width: widthPercentageToDP('95'),
    height: heightPercentageToDP('10'),
    backgroundColor: '#a9071a',
    borderRadius: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  burgerIconContainer: {
    justifyContent: 'center',
    width: widthPercentageToDP('10'),
    alignItems: 'center',
  },
  welcContainerNormal: {
    justifyContent: 'center',
    width: widthPercentageToDP('40'),
    height: heightPercentageToDP('10'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  welcStyle: {
    color: 'white',
    fontSize: 18,
  },
  userNStyle: {
    color: 'white',
    fontSize: 14,
  },
  buttonStyle: {
    fontSize: 20,
    color: '#a9071a',
    textAlign: 'center',
    padding: 2,
  },
  container1: {
    flexDirection: 'row',
    // borderWidth: 1,
    height: heightPercentageToDP('10'),
    position: 'absolute',
    top: heightPercentageToDP('2.5'),
    // left: widthPercentageToDP('1'),
    width: widthPercentageToDP('98'),
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  buttonContainer: {
    backgroundColor: '#a9071a',
    width: widthPercentageToDP('14'),
    height: heightPercentageToDP('8'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#ED1C25',
    fontSize: 21,
    padding: 10,
    textAlign: 'center',
  },
  readTextContainer: {
    // borderWidth: 1,
    width: widthPercentageToDP('90'),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: heightPercentageToDP('2'),
    paddingVertical: heightPercentageToDP('2'),
  },
  digitalSign: {
    width: widthPercentageToDP('90'),
    height: heightPercentageToDP('10'),
    // borderWidth: 1,
    color: '#ED1C25',
    backgroundColor: 'white',
    fontSize: 20,
    paddingHorizontal: widthPercentageToDP('5'),
  },
  buttonContainer: {
    backgroundColor: '#a9071a',
    width: widthPercentageToDP('50'),
    height: heightPercentageToDP('8'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: heightPercentageToDP('2'),
  },
});
