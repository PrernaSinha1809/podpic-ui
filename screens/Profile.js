import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/AntDesign';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Profile = ({route, navigation}) => {
  const [show, setShow] = useState(false);
  const [emailInput, setEmailInput] = useState(true);
  const [userInfo, setUserInfo] = useState();
  const [email, setEmail] = useState('');
  const [formData, setFormData] = useState({
    email: '',
    nchoice: 'password',
    password: '',
  });

  useEffect(() => {
    fetchData();
  });

  const fetchData = () => {
    AsyncStorage.getItem('User Data').then(value => {
      setUserInfo(JSON.parse(value));
    });
    // setFormData({...formData, email: userInfo.email});
  };

  const updateEmail = () => {
    if (email == '') {
      Alert.alert('Enter new email id!');
    } else if (email != '') {
      var data = JSON.stringify({
        email: email,
        username: userInfo.username,
      });

      var config = {
        method: 'post',
        url: 'http://208.82.115.154:8080/api/auth/updateProfile',
        headers: {
          'Content-Type': 'application/json',
        },
        data: data,
      };

      axios(config)
        .then(function (response) {
          console.log(JSON.stringify(response.data));
          Alert.alert(response.data);
          logout();
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  const logout = () => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    var raw = JSON.stringify({
      username: userInfo.username,
    });
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };
    fetch('http://208.82.115.154:8080/api/auth/signout', requestOptions)
      .then(response => response.json())
      .then(result => navigation.navigate('SignIn'))
      .catch(error => console.log('error', error));
  };

  const updatePswd = () => {
    console.log(formData);
    // showData();
    axios
      .post('http://208.82.115.154:8080/api/auth/rcrUsername', formData)
      .then(response => {
        // handle success
        Alert.alert(response.data);
        setShow(!show);
        logout();
      })
      .catch(error => {
        // handle error
        Alert.alert('Something Went Wrong!', error);
      });
  };

  if (userInfo) {
    return (
      <View style={styles.main}>
        <View style={styles.tab}>
          <View style={styles.backIcon}>
            <TouchableOpacity
              style={styles.backIconButton}
              onPress={() => navigation.goBack()}>
              <Icon name="back" size={25} color="#a9071a" />
            </TouchableOpacity>
          </View>
          <View style={styles.userName}>
            <Text style={styles.userNameStyle}>
              Welcome,
              <Text> </Text>
              {userInfo.username}
            </Text>
          </View>
          <View style={styles.backIconButton}>
            <TouchableOpacity
              onPress={() => logout()}
              style={{
                width: widthPercentageToDP('10'),
                height: heightPercentageToDP('9'),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon name="logout" size={25} color="#a9071a" />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.profileInfo}>
          <ScrollView contentContainerStyle={styles.ScrollView}>
            {/* <View > */}
            <View>
              <View>
                <Image
                  style={styles.profileImg}
                  source={require('../assets/images/profile.png')}
                />
              </View>
            </View>
            <View style={styles.infoContainer}>
              <Text style={styles.infoStyle}>
                Name:
                <Text> </Text>
                {userInfo.name}
              </Text>
            </View>
            <View style={styles.infoContainer}>
              {emailInput == true ? (
                <TouchableOpacity
                  onPress={() => {
                    setEmailInput(!emailInput),
                      setFormData({...formData, username: userInfo.username});
                  }}>
                  <Icon name="edit" size={25} color="#a9071a" />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    setEmailInput(!emailInput);
                    updateEmail();
                    // logout();
                  }}>
                  <Icon name="save" size={25} color="#a9071a" />
                </TouchableOpacity>
              )}

              <Text> </Text>
              <Text> </Text>
              {emailInput == true ? (
                <Text style={styles.infoStyle}>
                  Email:
                  <Text> </Text>
                  {userInfo.email}
                </Text>
              ) : (
                <TextInput
                  style={styles.input}
                  onChangeText={text => setEmail(text)}
                  placeholder="Enter new email id"
                  placeholderTextColor="#cccccc"
                />
              )}
            </View>
            <View style={styles.infoContainer}>
              <Text style={styles.infoStyle}>
                Role:
                <Text> </Text>
                {userInfo.roles[0]}
                {/* {userInfo.otherParam.roles[0]} */}
              </Text>
            </View>
            <View style={styles.updatePswd}>
              {show == false ? (
                <TouchableOpacity
                  onPress={() => {
                    setShow(!show),
                      setFormData({...formData, email: userInfo.email});
                  }}
                  style={[
                    styles.updatePswd,
                    {
                      height: heightPercentageToDP('8'),
                      backgroundColor: '#a9071a',
                    },
                  ]}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 20,
                      fontWeight: 'bold',
                    }}>
                    Update Password
                  </Text>
                </TouchableOpacity>
              ) : (
                <View
                  style={{
                    // borderWidth: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TextInput
                    style={styles.input}
                    onChangeText={text =>
                      setFormData({...formData, password: text})
                    }
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor="#cccccc"
                  />
                  <TouchableOpacity
                    onPress={() => {
                      // setFormData({...formData, email: userInfo.email}),
                      updatePswd();
                      // logout();
                    }}
                    style={[
                      styles.updatePswd,
                      {
                        width: widthPercentageToDP('50'),
                        height: heightPercentageToDP('8'),
                        backgroundColor: '#a9071a',
                        justifyContent: 'center',
                        alignItems: 'center',
                      },
                    ]}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 20,
                        fontWeight: 'bold',
                      }}>
                      Update
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  } else {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size="small" color="#0000ff" />
      </View>
    );
  }
};
export default Profile;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  tab: {
    position: 'absolute',
    top: heightPercentageToDP('2'),
    width: widthPercentageToDP('98'),
    height: heightPercentageToDP('8'),
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
    zIndex: 1,
  },
  ScrollView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  backIconButton: {
    width: widthPercentageToDP('10'),
    height: heightPercentageToDP('8'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  userName: {
    width: widthPercentageToDP('75'),
    height: heightPercentageToDP('8'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  userNameStyle: {
    fontSize: 25,
    color: '#a9071a',
    fontWeight: 'bold',
  },
  profileInfo: {
    // borderWidth: 1,
    width: widthPercentageToDP('98'),
    height: heightPercentageToDP('80'),
    justifyContent: 'center',
    alignItems: 'center',
    // marginVertical: heightPercentageToDP('10')
  },
  infoStyle: {
    fontSize: 17,
    color: 'black',
    // fontWeight: 'bold',
    textAlign: 'center',
    // marginHorizontal: widthPercentageToDP('2')
  },
  infoContainer: {
    // borderWidth: 1,
    width: widthPercentageToDP('95'),
    height: heightPercentageToDP('10'),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  updatePswd: {
    // borderWidth: 1,
    width: widthPercentageToDP('80'),
    // height: heightPercentageToDP('8'),
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: heightPercentageToDP('1'),
  },
  profileImg: {
    // borderWidth: 1,
    width: widthPercentageToDP('35'),
    height: heightPercentageToDP('20'),
    resizeMode: 'contain',
    borderRadius: 120,
    marginVertical: heightPercentageToDP('2'),
  },
  input: {
    height: heightPercentageToDP('8'),
    width: widthPercentageToDP('70'),
    color: '#a9071a',
    // margin: 12,
    borderWidth: 1,
  },
});
