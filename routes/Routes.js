import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import SplashScreen from '../screens/SplashScreen';
import SignIn from '../screens/SignIn';
import SignUp from '../screens/SignUp';
import WelcomePage from '../screens/WelcomePage';
// import FrgtUsername from '../screens/FrgtUsername';
import PDFViewer from '../screens/PDFViewer';
import StatusHandle from '../screens/StatusHandling';
import TransportImages from '../screens/TransporterImage';
import ForgotUserPwd from '../screens/components/sample';

import Profile from '../screens/Profile';

import OrderHistory from '../screens/OrderHistory';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignIn"
          component={SignIn}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="SignUp"
          component={SignUp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Welcome Page"
          component={DrawerNav}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ForgotUserPwd"
          component={ForgotUserPwd}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PDFViewer"
          component={PDFViewer}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="StatusHandle"
          component={StatusHandle}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="TransportImages"
          component={TransportImages}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

const DrawerNav = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Welcome Page"
      drawerContentOptions={{
        activeTintColor: '#a9071a',
      }}>
      <Drawer.Screen name="Welcome Page" component={WelcomePage} />
      <Drawer.Screen name="Profile" component={Profile} />
      <Drawer.Screen name="Order History" component={OrderHistory} />
    </Drawer.Navigator>
  );
};
