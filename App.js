import React from 'react';
import { Text, View } from 'react-native';
import Routes from './routes/Routes'

const App = () => {
  return (
    <Routes />
  );
}

export default App;